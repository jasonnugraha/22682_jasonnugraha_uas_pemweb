<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Memberships;
use Illuminate\Support\MessageBag;

class MembershipsController extends Controller
{

    function index()
    {
        $memberships = Memberships::all();
        return view('memberships', compact('memberships'));
    }

    function postHandling(Request $request)
    {
        $options = Memberships::getPossibleStatuses();
        $command = $request['command'];
        if ($command == 'Update') {
            $id = $request['idUpdate'];
            $membershipUpdate = Memberships::find($id);
            // return view('form', compact('membershipUpdate', 'command', 'options'));
            return view('form', ['membershipUpdate' => $membershipUpdate, 'command' => $command, 'options' => $options]);
        } else if ($command == 'Insert') {
            return view('form', compact('command', 'options'));
        } else if ($command == 'Delete') {
            $id = $request['idUpdate'];
            $membershipDelete = Memberships::find($id);
            return view('deleteConfirmation', compact('id', 'membershipDelete'));
        }
    }

    function updateHandling(Request $request)
    {
        if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
            $errors = new MessageBag;
            $errors->add("submission_error", "Unknown Captcha Verification!");
            return view('errorHandling')->withErrors($errors);
        } else {
            $verifyURL = "https://www.google.com/recaptcha/api/siteverify";
            $verifyURL .= "?secret=6LdqRq0UAAAAAHtodO_kbnAwvO-UMopL-U4tT7wX";
            $verifyURL .= "&response=" . $request["g-recaptcha-response"];
            $verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
            $verifyResponse = file_get_contents($verifyURL);
            $responseParse = (array) json_decode($verifyResponse);
            if ($responseParse["success"] == false) {
                $errors = new MessageBag;
                $errors->add("submission_error", "CAPTCHA Validation Error!!!");
                return view('errorHandling')->withErrors($errors);
            }
        }
        $validateData = $request->validate([
            'name' => 'bail|required|string',
            'nim' => 'bail|required|digits:11',
            'division' => 'bail|required|string',
            'prodi' => 'bail|required|string',
            'year' => 'bail|required|digits:4',
        ]);

        $id = $request['idUpdate'];
        $oldMember = Memberships::find($id);
        $oldMember->name = $request['name'];
        $oldMember->nim = $request['nim'];
        $oldMember->division = $request['division'];
        $oldMember->prodi = $request['prodi'];
        $oldMember->year = $request['year'];
        $oldMember->save();
        return $this->index();
    }

    function insertHandling(Request $request)
    {
        if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
            $errors = new MessageBag;
            $errors->add("submission_error", "Unknown Captcha Verification!");
            return view('errorHandling')->withErrors($errors);
        } else {
            $verifyURL = "https://www.google.com/recaptcha/api/siteverify";
            $verifyURL .= "?secret=6LdqRq0UAAAAAHtodO_kbnAwvO-UMopL-U4tT7wX";
            $verifyURL .= "&response=" . $request["g-recaptcha-response"];
            $verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
            $verifyResponse = file_get_contents($verifyURL);
            $responseParse = (array) json_decode($verifyResponse);
            if ($responseParse["success"] == false) {
                $errors = new MessageBag;
                $errors->add("submission_error", "CAPTCHA Validation Error!!!");
                return view('errorHandling')->withErrors($errors);
            }
        }

        $validateData = $request->validate([
            'name' => 'bail|required|string',
            'nim' => 'bail|required|digits:11',
            'division' => 'bail|required|string',
            'prodi' => 'bail|required|string',
            'year' => 'bail|required|digits:4',
        ]);

        $newMember = new Memberships;
        $newMember->name = $request['name'];
        $newMember->nim = $request['nim'];
        $newMember->division = $request['division'];
        $newMember->prodi = $request['prodi'];
        $newMember->year = $request['year'];
        $newMember->save();

        return $this->index();
    }

    function deleteHandling(Request $request)
    {
        if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
            $errors = new MessageBag;
            $errors->add("submission_error", "Unknown Captcha Verification!");
            return view('errorHandling')->withErrors($errors);
        } else {
            $verifyURL = "https://www.google.com/recaptcha/api/siteverify";
            $verifyURL .= "?secret=6LdqRq0UAAAAAHtodO_kbnAwvO-UMopL-U4tT7wX";
            $verifyURL .= "&response=" . $request["g-recaptcha-response"];
            $verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
            $verifyResponse = file_get_contents($verifyURL);
            $responseParse = (array) json_decode($verifyResponse);
            if ($responseParse["success"] == false) {
                $errors = new MessageBag;
                $errors->add("submission_error", "CAPTCHA Validation Error!!!");
                return view('errorHandling')->withErrors($errors);
            }
        }
        if ($request['confirm'] == 'Yes') {
            $id = $request['idUpdate'];
            $deletedProduct = Memberships::find($id);
            $deletedProduct->delete();
        }
        return $this->index();
    }
}
