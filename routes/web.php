<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MembershipsController@index');

Auth::routes();

Route::get('/home','MembershipsController@index');

Route::post('/formPage','MembershipsController@postHandling');

Route::post('/updateData','MembershipsController@updateHandling');
Route::post('/insertData','MembershipsController@insertHandling');
Route::post('/deleteData','MembershipsController@deleteHandling');
Route::get('/formPage',function(){
    return view('errorHandling');
});
Route::post('/backToHome','MembershipsController@index');
// Route::get('/home', 'HomeController@index')->name('home');
