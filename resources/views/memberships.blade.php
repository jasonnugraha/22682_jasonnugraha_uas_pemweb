@extends('defaultApp')
@section('tableMember')
<div class="container table-responsive">
    <form action="formPage" method="post">
        <table id="table" class="table table-striped table-hover">
            <thead>
                @auth
                <th>Select</th>
                @endauth
                <th>Name</th>
                <th>NIM</th>
                <th>Division</th>
                <th>Prodi </th>
                <th>Year</th>
            </thead>
            <tbody>
                @foreach($memberships as $membership)
                <tr>
                    @auth
                    <td class="text-center">
                        <p>
                            <label>
                                <input class="with-gap" name="idUpdate" value="{{$membership->id}}" type="radio" />
                                <span></span>
                            </label>
                        </p>
                    </td>
                    @endauth
                    <td>{{$membership->name}}</td>
                    <td>{{$membership->nim}}</td>
                    <td>{{$membership->division}}</td>
                    <td>{{$membership->prodi}}</td>
                    <td>{{$membership->year}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @auth
        <!-- <input type="submit" value="Update" name="command" class="btn waves-effect waves-light"> -->
        <button class="btn waves-effect waves-light" type="submit" name="command" value="Insert">Insert
            <i class="material-icons right">insert_drive_file</i>
        </button>
        <button class="btn waves-effect waves-light" type="submit" name="command" value="Update">Update
            <i class="material-icons right">update</i>
        </button>
        <button class="btn waves-effect waves-light" type="submit" name="command" value="Delete">Delete
            <i class="material-icons right">delete</i>
        </button>
        <!-- <input type="submit" value="Delete" name="command" class="btn btn-danger">

        <input type="submit" value="Insert" name="command" class="btn btn-primary"> -->
        @csrf
        @endauth
    </form>
</div>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    });
</script>
@endsection