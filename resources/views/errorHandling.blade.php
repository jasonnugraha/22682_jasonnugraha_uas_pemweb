@extends('defaultApp')
@section('errorHandling')
@if($errors->any())
<div class="container">
    <div class="alert alert-danger">
    <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    </ul>
    </div>

    <form id="errorForm" action="backToHome" method="post" name="errorForm" >
    @csrf
    <input type="submit" value="Back To Home" class="btn btn-primary" name="error">
    </form>
</div>
@endif
@endsection