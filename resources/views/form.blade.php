@extends('defaultApp')
@section('form')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<div class="container">
    <h4>{{$command ?? "Insert"}} Form</h4>
    <form action="{{$command == 'Insert' ? 'insertData' : 'updateData'}}" method="post">
        @csrf
        <div class="form-group col-5">
            <input type="hidden" name="idUpdate" value="{{$membershipUpdate->id ?? ''}}">
            <label for="Name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$membershipUpdate->name ?? ''}}" placeholder="Enter Name">
        </div>
        <div class="form-group col-5">
            <label for="nim">NIM</label>
            <input type="text" class="form-control" id="nim" name="nim" value="{{$membershipUpdate->nim ?? ''}}" placeholder="Enter NIM">
        </div>
        <div class="form-group col-5">
            <label for="division">Division</label>
            <input type="text" class="form-control" id="division" name="division" value="{{$membershipUpdate->division ?? ''}}" placeholder="Enter Division">
        </div>
        <div class="form-group col-5">
            <label>Prodi Lama</label>
            <h6>{{$membershipUpdate->prodi ?? 'Belum Terpilih'}}</h6>
        </div>
        <div class="form-group col-5">
            <label for="prodi">Prodi Baru</label>
            <select name="prodi" class="custom-select custom-select-lg" value="{{$membershipUpdate->prodi}}">
                <option value="">Select Prodi</option>
                @foreach($options as $option)
                <option value="{{$option}}" @if ($option == $membershipUpdate->prodi) selected @endif>{{ $option }}</option>
                @endforeach
            </select>
            <!-- <input type="text" class="form-control" id="prodi" name="prodi" value="{{$membershipUpdate->prodi ?? ''}}" placeholder="Enter Prodi"> -->
        </div>
        <div class="form-group col-1">
            <label for="year">Year</label>
            <input class="date-own form-control" style="" type="text" id="year" value="{{$membershipUpdate->year ?? ''}}" name="year">
            <script type="text/javascript">
                $('.date-own').datepicker({
                    minViewMode: 2,
                    format: 'yyyy'
                });
            </script>
        </div>
        <div class="g-recaptcha" data-sitekey="6LdqRq0UAAAAAOv_M7nYX6jRfVZV-z_QDtuGrz9c" style="margin-bottom: 10px;"> </div>
        <div class="form-group col-1">
            <button class="btn waves-effect waves-light" style="margin:10px;" type="submit" name="command" value="Submit">Submit
                <i class="material-icons right">send</i>
            </button>
        </div>

    </form>

</div>
@endsection