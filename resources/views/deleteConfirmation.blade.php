@extends('defaultApp')
@section('deleteConfirmation')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="container">
    @if(isset($membershipDelete->id))
    <form action="deleteData" method="post">
        @csrf
        <input type="hidden" name="idUpdate" value="{{$id}}">
        <h4>Are You Sure want to delete this data</h4>

        <div class="form-group">
            <label for="Name">Name</label>
            <p>{{$membershipDelete->name}}
                <p>
        </div>
        <div class="form-group">
            <label for="NIM">NIM</label>
            <p>{{$membershipDelete->nim}}
                <p>
        </div>
        <div class="form-group">
            <label for="Division">Division</label>
            <p>{{$membershipDelete->division}}
                <p>
        </div>
        <div class="form-group">
            <label for="Prodi">Prodi</label>
            <p>{{$membershipDelete->prodi}}
                <p>
        </div>
        <div class="form-group">
            <label for="Year">Year</label>
            <p>{{$membershipDelete->year}}
                <p>
        </div>
        <div class="g-recaptcha" data-sitekey="6LdqRq0UAAAAAOv_M7nYX6jRfVZV-z_QDtuGrz9c" style="margin-bottom: 10px;"> </div>
        <div>
            <button class="btn waves-effect waves-light" style="" type="submit" name="confirm" value="Yes">Yes
                <i class="material-icons right">check</i>
                <button class="btn waves-effect waves-light" style="margin:30px;" type="submit" name="confirm" value="No">No
                    <i class="material-icons right">not_interested</i>
        </div>
    </form>

</div>
@else
<form action="deleteData" method="post">
    <h1>Please Choose Data To Delete</h1>
    <button class="btn waves-effect waves-light" style="margin:30px;" type="submit" name="confirm" value="No">No
        <i class="material-icons right">not_interested</i>
</form>
@endif

@endsection