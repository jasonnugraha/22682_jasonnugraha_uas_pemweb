<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191);
            $table->string('nim',191)->unique();
            $table->string('division',191);
            $table->enum('prodi', ['Akutansi','Arsitektur','Desain Komunikasi Visual','Film','Jurnalistik','Komunikasi Strategis','Perhotelan', 'Sistem Informasi','Informatika','Teknik Komputer','Teknik Fisika','Teknik Elektro','Manajemen']);
            $table->smallInteger('year');
            // $table->timestamps();
        });
        DB::statement("ALTER TABLE memberships ADD cover_image  MEDIUMBLOB");
        DB::statement("ALTER TABLE memberships ADD qr_code  MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership');
    }
}